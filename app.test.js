/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const nock = require('nock');
const request = require('supertest');
const app = require('./server/app');

describe.skip('The server should', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  test('return the latest blockchain', () => {
    const data = {
      'hash': '0000000000000000003361f1799ee33186fbe42ffde06fcc95208398584abbac',
      'time': 1526920241,
      'block_index': 1701967,
      'height': 523704,
      'txIndexes': [
        349640346,
        349640145
      ]
    };

    nock('https://blockchain.info/')
      .get('/latestblock')
      .reply(200, data);

    request(app)
      .get('/latest')
      .expect(200)
      .then(res => {
        expect(JSON.parse(res.text)).toHaveProperty('hash');
      });
  });
});

