To run:
```
npm install
npm run dev
```

Start webpack into another window to compile the bundle:
```
npm run build
```

##### To dos:
* Error boundary component;
* Add flow;
* Add componentDidCatch;
* Add caching;
* 
* Deploy!

##### Stack:
* Node.JS;
* Express.JS;
* React;
* React Router;
* Redux;
* Webpack;
* Reselect;
* Redux Thunk.

##### Linting:
* Eslint
* Husky - for painless pre-commit hooks.
