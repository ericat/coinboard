// @flow

import * as React from 'react';
import { Provider } from 'react-redux';
import { fetchTransactions } from '../redux/actions';

import App from '../components/App';
import createStore from '../redux/createStore';

const fetchData = require('../api').fetchData;

class Index extends React.Component {
  static getInitialProps() {
    return fetchData('http://localhost:8082/latest')
      .then(res => res)
      .catch(err => {
        console.error(err, 'getInitialProps');
      });
  }

  constructor(props) {
    super(props);
    const { hash, time, height } = props;
    this.store = createStore({ hash, time, height });

    if (hash) {
      this.store.dispatch(fetchTransactions(hash));
    }
  }

  render() {
    return (
      <Provider store={this.store}>
        <App />
      </Provider>
    );
  }
}

export default Index;
