/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import * as React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import App from '../App';

jest.unmock('../App');

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('App component', () => {
  const store = mockStore({
    height: 523699,
    n_tx: 2750,
    relayed_by: '0.0.0.0',
    time: 1526916646,
    info: {
      tx: [
        {time: 1526916646, relayed_by: '0.0.0.0', inputs: [], out: []}
      ]}
  });

  it('Renders correctly', () => {
    const el = renderer.create(
      <Provider store={store}>
        <App />
      </Provider>
    );
    const tree = el.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
