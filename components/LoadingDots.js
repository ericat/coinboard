import * as React from 'react';

const LoadingDots = () => (
  <div className="loading">
    <span className="dot"></span>
    <span className="dot"></span>
    <span className="dot"></span>
  </div>
);

export default LoadingDots;

