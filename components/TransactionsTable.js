import * as React from 'react';
import { connect } from 'react-redux';
import format from 'date-fns/format';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import { computedTxSelector } from '../redux/selectors';

// Note(ericat): from Mozilla
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
const precisionRound = (number, precision) => {
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
};

const TransactionsTable = (props) => {
  const columns = [{
    Header: props => 'Relayed By',
    accessor: 'relayed_by'
  }, {
    Header: 'Time',
    accessor: 'time',
    Cell: props => <span>{format(props.value, 'M/D/YYYY')}, {format(props.value, 'HH:mma')}</span>
  }, {
    Header: 'Inputs',
    accessor: 'inputs'
  }, {
    Header: 'Outputs',
    accessor: 'out'
  }, {
    Header: 'Ratio',
    accessor: 'ratio',
    Cell: props => <span>{precisionRound(props.value, 1)}:1</span>
  }, {
    Header: 'Value',
    accessor: 'value'
  }];

  return (
    <div className="table-container">
      <ReactTable
        data={props.tx}
        columns={columns}
      />
    </div>);
};

const mapStateToProps = ({ info }) => ({
  tx: computedTxSelector(info)
});

export default connect(
  state => mapStateToProps(state)
)(TransactionsTable);
