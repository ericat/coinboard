import * as React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import LoadingDots from './LoadingDots';

const LoadingTable = (props) => {
  return (
    <div className="table-container">
      <div className="ReactTable">
        <div className="rt-table" role="grid">
          <div className="rt-thead -header" style={{ 'minWidth': '600px', 'color': '#9E9E9E'}}>
            <div className="rt-tr" role="row">
              <div className="rt-th  rt-resizable-header"  style={{'flex': '100 0 auto', 'width': '100px'}}>
                <div className="rt-resizable-header-content">Relayed By</div>
              </div>
              <div className="rt-th  rt-resizable-header"  style={{'flex': '100 0 auto', 'width': '100px'}}>
                <div className="rt-resizable-header-content">Time</div>
              </div>
              <div className="rt-th  rt-resizable-header"  style={{'flex': '100 0 auto', 'width': '100px'}}>
                <div className="rt-resizable-header-content">Inputs</div>
              </div>
              <div className="rt-th  rt-resizable-header"  style={{'flex': '100 0 auto', 'width': '100px'}}>
                <div className="rt-resizable-header-content">Outputs</div>
              </div>
              <div className="rt-th  rt-resizable-header"  style={{'flex': '100 0 auto', 'width': '100px'}}>
                <div className="rt-resizable-header-content">Ratio</div>
              </div>
              <div className="rt-th  rt-resizable-header"  style={{'flex': '100 0 auto', 'width': '100px'}}>
                <div className="rt-resizable-header-content">Value</div>
              </div>
            </div>
          </div>
          <div className="table-inner">
            <LoadingDots />
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoadingTable;


