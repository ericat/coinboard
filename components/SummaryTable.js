import * as React from 'react';
import format from 'date-fns/format';

const SummaryTable = (props) => {
  const {
    hash,
    height,
    time
  } = props;

  return (
    <div className="table-container">
      <div className="overview">
        <div className="row">
          <p>Latest Hash</p>
          <p>{hash}</p>
        </div>
        <div className="row">
          <p>Height</p>
          <p>{height}</p>
        </div>
        <div className="row">
          <p>Time</p>
          <p>{format(time, 'M/D/YYYY')}, {format(time, 'HH:mma')}</p>
        </div>
      </div>
    </div>);
};

export default SummaryTable;
