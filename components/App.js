import * as React from 'react';
import { connect } from 'react-redux';

import SummaryTable from './SummaryTable';
import TransactionsTable from './TransactionsTable';
import LoadingTable from './LoadingTable';

import './reset.css';
import './style.css';

const App = (props) => {
  return (
    <React.Fragment>
      <SummaryTable {...props} />
      {props && props.loading ?
        <LoadingTable /> : <TransactionsTable />
      }
    </React.Fragment>
  );
};

export default connect(
  state => state
)(App);
