const path = require('path');
const express = require('express');
const next = require('next');
const compression = require('compression');

const fetchData = require('../api/').fetchData;

const baseUrl = 'https://blockchain.info/';
const latestUrl = `${baseUrl}latestblock`;
const transactionsUrl = `${baseUrl}rawblock`;

const handleRes = (res, fields) => {
  return JSON.stringify(res, fields);
};

const txFields = [
  'time',
  'height',
  'n_tx',
  'tx',
  'relayed_by',
  'inputs',
  'out',
  'value'
];

const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());

app.get('/latest', (req, res) => {
  return fetchData(latestUrl)
    .then(response => res.send(handleRes(response, ['hash', 'time', 'height'])))
    .catch(err => {
      throw new Error(err);
    });
});

app.get('/transactions', (req, res) => {
  return fetchData(`${transactionsUrl}/${req.query &&req.query.hash}?format=json`)
    // Note(Ericat): remove unneeded fields
    .then(json => res.send(handleRes(json, txFields)))
    .catch(err => {
      throw new Error(err);
    });
});

app.get('*', (req, res) => handle(req, res));

module.exports.app = app;
module.exports.nextApp = cb => {
  nextApp
    .prepare()
    .then(() => cb())
    .catch(err => {
      console.error('Catch: error in server', err);
      process.exit(1);
    });
};
