const { app, nextApp } = require('./app');

const port = parseInt(process.env.PORT, 10) || 8082;
module.exports = nextApp(() => {
  app.listen(port, () => {
    console.log(`Server started on port http://localhost:${port}`);
  });
});
