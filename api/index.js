const fetch = require('isomorphic-fetch');

const checkStatus = res => {
  if (res.status >= 200 && res.status < 300) {
    return res;
  } else {
    const error = new Error(res.statusText);
    error.res = res;
    throw error;
  }
};

const fetchData  = (url) => {
  return fetch(url)
    .then(checkStatus)
    .then(json => json.json())
    .catch(err => {
      throw new Error(err);
    });
};

module.exports = {
  fetchData,
  checkStatus
};

