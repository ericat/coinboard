/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { computedTxSelector } from '../../redux/selectors';

let exampleState = {
  tx: [
    {time: 1526916646, relayed_by: '0.0.0.0', inputs: [1,2,3], out: [{value:2}, {value: 3}]}
  ]
};

describe('Computed selector should', () => {
  it('parse the example state', () => {
    const computed = computedTxSelector(exampleState)[0];
    expect(computed.inputs).toEqual(3);
    expect(computed.ratio).toEqual(1.5);
    expect(computed.out).toEqual(2);
  });
});
