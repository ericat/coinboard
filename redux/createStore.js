// @flow

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import App from '../components/App';
import rootReducer from './reducers';

export default ({ hash, time, height, loading = true }) => {
  const store = createStore(
    rootReducer,
    { hash, time, height, loading },
    applyMiddleware(thunk)
  );
  return store;
};

