export default (state, action) => {
  switch (action.type) {
  case 'FETCH_SUCCESS':
    return {
      ...state,
      hash: action.val.hash,
      info: action.val.info,
      loading: false,
    };
  case 'FETCH_ERROR':
    return {
      ...state,
      error: action.text,
      message: action.text,
    };
  default:
    return state;
  }
};
