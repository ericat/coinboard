import { createSelector } from 'reselect';

// TODO: fix weird bug where out is a number
const txSelector = state => state.tx;
const inSelector = inputs => inputs.length || 1;
const outSelector = out => out.length || 1;
const valueSelector = out => {
  return Array.isArray(out) ?
    out.reduce((acc, item) => acc + item.value, 0) :
    out;
};

const ratioSelector = createSelector(
  (input, out) => input / out
);

export const computedTxSelector = createSelector(
  txSelector,
  tx => tx.map(item => {
    item.inputs = inSelector(item.inputs);
    item.out = outSelector(item.out);
    item.value = valueSelector(item.out); // TODO convert to $$
    item.ratio = item.inputs / item.out;
    return item;
  })
);
