import fetch from 'isomorphic-fetch';

import { fetchData } from '../api/';
import { computedTxSelector } from './selectors';

const baseUrl = 'https://blockchain.info/';
const latestUrl = `${baseUrl}latestblock`;

/*eslint-disable no-unused-vars*/
const FETCH_SUCCESS = 'FETCH_SUCCESS';
const FETCH_ERROR = 'FETCH_ERROR';
/*eslint-enable no-unused-vars*/

const fetchSuccess = (res, hash) => ({
  type: 'FETCH_SUCCESS',
  val: {
    info: res,
    hash
  }
});

export const fetchTransactions = (hash) => {
  return (dispatch, getState) => {
    return fetchData(`/transactions/?hash=${hash}`)
      .then(res => {
        dispatch(fetchSuccess(res, hash));
      }).catch(err => {
        dispatch({
          type: 'FETCH_ERROR',
          val: err
        });
      });
  };
};
